package com.oreillyauto;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**

Your task is to complete the game "Connect Four." - You are provided a
skeleton to speed up the program completion. - Please refer to
https://www.wikihow.com/Play-Connect-4 for - game rules. - Use X to represent
the Red Player - Use Y to represent the Blue Player - Reference Helper.java
as some of the helper (stubbed) functions will assist (speed up) your
development. */
public class ConnectFour {
    public boolean gameOver = false; // Game Over boolean
    public boolean gameStarted = false; // Game Started boolean
    public static BufferedReader consoleReader;// Console Reader to take input from user
    public int attempts = 0; // Number of tries/attempts for both players (42 max)
    public String message = ""; // Message to the user
    public String option = ""; // Current selected user option (Play Game, Quit etc...)
    public ConnectFour connectFour; // Class object
    public String turn = "X"; // Starts with Red Player by default
    public Map<Point, String> gameBoard = new HashMap<Point, String>(); // Stores all the pieces NEW
    public final int MAX_ROWS = 6; // Number of rows
    public final int MAX_COLUMNS = 7; // Number of columns

/**
 * Connect Four - Constructor The connect four constructor initializes the game
 * and places the user in a game loop.
 */
public ConnectFour() {
    // Declare Variables
    // Get access to the ConnectFour class we created via "this" because
    // we pass the connectFour object to the Helper class in some cases.
    connectFour = this;

    // BufferedReader is synchronous while Scanner is not.
    // BufferedReader has significantly larger buffer memory than Scanner.
    consoleReader = new BufferedReader(new InputStreamReader(System.in));

    // Set initial message to the user
    Helper.setMessage(option, gameOver, gameStarted, connectFour);

    // Game Loop - draw the board, get user selection, and provide feedback
    while (true) {
        // Draw the board (menu, connectfour area)
        drawBoard();

        // Get the user selection
        Object userSelection = Helper.getUserOption(consoleReader);

        // Process the user selection
        // Numeric = 1 -> 7
        // Alpha = S or Q
        if (userSelection instanceof Integer) {
            // If the user entered a number, then we need to
            // process this selection. It is most likely a 
            // position on the board. 
            // 1 <= userSelection <= 7
            processNumericSelection((Integer)userSelection);
        } else {
            // If the user did NOT enter a number, they
            // must have entered a letter from the menu. 
            // They might want to Start/Restart or Quit
            option = (String)userSelection;
            processAlphaSelection();
        }
        option = "";
    }
}

/**
 * In this method, we need to figure out what the user wants to do with the menu
 * option that they selected. Notice that option is a global variable. We can
 * assess what to do for a given input based on the option selected.
 * 
 * @param userSelection
 */
public void processNumericSelection(Integer userSelection) {

    switch (userSelection) {
    case 1:
        // Submit a letter
        option = "1";
        playMove(0);
        break;
    case 2:
        // Guess the word
        option = "2";
        playMove(1);
        break;
    case 3:
        // Give up
        option = "3";
        playMove(2);
        break;
    case 4:
        // Quit
        option = "4";
        playMove(3);
        break;
    case 5:
        // Quit
        option = "5";
        playMove(4);
        break;
    case 6:
        // Quit
        option = "6";
        playMove(5);
        break;
    case 7:
        // Quit
        option = "7";
        playMove(6);
        break;
    default:
        message = ""; // set error message (for invalid selection?)
    }

}

/**
 * Process Alpha Selection The user should have selected a letter from the menu.
 * We need to process their selection.
 */
private void processAlphaSelection() {
    String temp = option.toLowerCase();

    switch (temp) {
    case "s": // User selected option S - start/restart the game
        System.out.println("Game Started!");
        break;
    case "q": // User selected option 2 (time to go)
        System.out.println("Goodbye!");
        System.exit(0);
        break;
    default: // WTC
        System.out.println("Invalid Selection!");
        break;
    }
}

/**
 * Play Move You will need to ensure that this column is not already full. If
 * not, place the proper game piece into the proper position on the board.
 * 
 * @param selection
 */
public void playMove(int selection) {

    // Iterates through the number of rows (size of the column) and adds a point key if it doesn't exist.
    // Throws message if column is full
    for (int i = 0; i < MAX_ROWS; i++) {
        if (!gameBoard.containsKey(new Point(selection, i))) {
            gameBoard.put(new Point(selection, i), turn);
            turn = turn.equals("X") ? "Y" : "X";
            attempts++;
            break;
        }
        
        if (i >= MAX_ROWS - 1) {
            message = "Column " + i + " is full, pick a different column";
        }
    }

    isGameOver();
}

/**
 * Is Game Over In this method, we need to determine if all positions have been
 * filled or a user has connected four game pieces.
 * 
 * @return
 */
private boolean isGameOver() {
    if(attempts == 42) {
        return true;
    }
    
    
    else {
        return false;
    }
}

/**
 * Set Message Our message variable is global. This method is global. We can
 * call this method from the Helper method when applicable.
 * 
 * @param message
 */
public void setMessage(String message) {
    this.message = message;
}

/**
 * Draw Board This method draws the board in the console. The Helper method
 * calls a system command to clear the console. This does NOT work in Eclipse
 * but works well in the Windows console. Check out the document "Creating a
 * runnable jar" in the Google Drive. If you run your Connect Four application
 * from a windows console, your console will clear successfully when the board
 * is redrawn.
 */
private void drawBoard() {
    // Clear the console, set messages in context, build the underscore string
    Helper.clearTheConsole();
    Helper.setMessage(option, gameOver, gameStarted, connectFour);

    // Do not remove the empty println statement. The clearTheConsole() function
    // call adds an odd character in Eclipse. We will build a new line.
    System.out.println("");

    // By default, the entire Connect Four board is drawn to the console.
    // You will need to provide a way to show X and O for each of the user's
    // selected moves.
    System.out.println("************* ConnectFour 1.0 ************* \n");
    System.out.println("                           1 2 3 4 5 6 7");
    System.out.println("                         __ _ _ _ _ _ _ __");
    System.out.print("Options:                 |");
    drawRow(6);
    System.out.print("S. Start/Restart a game  |");
    drawRow(5);
    System.out.print("Q. Quit                  |");
    drawRow(4);
    System.out.print("                         |");
    drawRow(3);
    System.out.print("                         |");
    drawRow(2);
    System.out.print("                         |");
    drawRow(1);
    System.out.print("                         + ============= +");
    System.out.println("");
    System.out.print("                         ||_____________||");
    System.out.println("");
    System.out.println(message);

    // Clear the message after we show it
    message = "";
}

/**
 * Draw ConnectFour Body Parts The number of fails is global so we can use this
 * information in order to know how to draw the connectfour.
 * 
 * @param bodyPart
 */
private void drawRow(int row) {
    StringBuilder strBuilder = new StringBuilder(""); 
    
    // Grabs player piece if point exists and append to string builder
    for (int i = 0; i < MAX_COLUMNS; i++) {
        if (gameBoard.containsKey(new Point(i, row - 1))) {
            strBuilder.append(" " + gameBoard.get(new Point(i, row - 1)));
        }
        else {
            strBuilder.append("  ");
        }
    }
    strBuilder.append(" |");
    
    System.out.println(strBuilder);
    
    /*
     * switch (row) { case 1: System.out.println("               |"); break; case 2:
     * System.out.println("               |"); break; case 3:
     * System.out.println("               |"); break; case 4:
     * System.out.println("               |"); break; case 5:
     * System.out.println("               |"); break; case 6:
     * System.out.println("               |"); break; }
     */
}

/**
 * Main - Entry point into the application
 * 
 * @param args
 */
public static void main(String[] args) {
    new ConnectFour();
}
}