package com.oreillyauto;

import java.io.BufferedReader;

/**
 * This class is designed to help you add logic to your application. Notice
 * that the methods are static. You can access the methods by calling the 
 * class and static method without instantiating the Helper class:
 *    Helper.isNumeric(String string);
 *    
 * Whenever you see "ConnectFour connectFour" as a parameter, this is an indication 
 * that you can make method calls back to the ConnectFour class. This communication
 * is often necessary.
 *    connectFour.setMessage("Game Over!");
 * 
 * @author jbrannon5
 */
public class Helper {

	/**
	 * Set Message
	 * This method should contain the logic to decide what 
	 * message we need to provide back to the user.
	 * @param option
	 * @param gameOver
	 * @param gameStarted
	 * @param connectFour
	 */
    public static void setMessage(String option, boolean gameOver, boolean gameStarted, 
    		ConnectFour connectFour) {
    	
        // Set a user message based on the current user selected option
        // e.g. connectFour.setMessage("");
        
        if (option.length() == 0) {
            // If option is 0 then no user option is selected (yet)
        	connectFour.setMessage("Please select an option: ");
        } else {
            // Manage Numeric Options
            if (isNumeric(option)) {
                connectFour.processNumericSelection(Integer.parseInt(option));
            } else {
                // Manage Non-Numeric Options (S and Q)
            }
        }
    }

    /**
     * Is Numeric
     * Checks if the string that is passed in is an Integer or not
     * @param option
     * @return
     */
    private static boolean isNumeric(String string) {
        try {
            Integer.parseInt(string);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Clear the Console
     * This method works great in the Windows console. It does NOT
     * clear the console in Eclipse unfortunately. It, instead, will
     * print a weird character. =>    
     */
    public static void clearTheConsole() {
        try {
            Thread.sleep(500);
            new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Get User Option
     * This method uses the BufferedReader class to read the console. 
     * @param consoleReader
     * @return
     */
    public static Object getUserOption(BufferedReader consoleReader) {
        try {
            String response = consoleReader.readLine();
            
            if (response != null) {
                return (Helper.isNumeric(response)) ? Integer.parseInt(response) : response;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
